Goals of this project...

I want a subjectively "balanced experience" the "balanced" game mechanics I'm going for are:

Exploration: I want the world to be interesting
	- We want ways to get around the world as well.
	
This has been covered by a few mods already:
cloudlands (hallelujah mountains)
dfcaverns (dwarf fortress style caverns)
glider (the "realistic" glider mod)
meseportals (so you can link distant lands together easier, also I liked this one the best of all the options)

Automation:
	I want the player to be able to farm things. I'm still playing with mods and figuring out what I want exactly... But here's what I have so far.
	- mesecons

under considerations
	pipeworks
	technic
	hoppers

Decoration:
	I haven't added anything here yet. I'm considering the "home decor" mod
	
Combat?:
	Haven't thought about this. PVE and PVP should be considered. I don't know if I'll have it or not

Misc Player Experience:
	things like having a good crafting guide, or having torches glow while they are in your hand, or 
	
	Things we have: 
		- builtin_item (makes items influencible by water physics)
		- show_wielded_item (shows the name of the item you've selected)
		- wielded_light - if the item in your hand emits light, it will emit light by being in your hand
		- head_anim (5.3+ only) shows the direction the player is looking.
		- skinsdb
	
