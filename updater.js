/**
 * @typedef SubDir
 * @type {"environment"|"misc"|"player"|undefined}
 */

/**
 * @typedef MTMod
 * @type {object}
 * @property {string} name The folder name of the mod
 * @property {string} archiveURL The repo it came frome
 * @property {SubDir} modSubdir The subdirectory in the mods folder to put it in
 */

 /**
  * shorthand for creating an MTMod object
  * @param {string} name The folder name of the mod
  * @param {string} archiveURL The repo it came frome
  * @param {SubDir} modSubdir The subdirectory in the mods folder to put it in
  * @returns {MTMod}
  */
function M(name, archiveURL, modSubdir)
{
  return { name: name, archiveURL: archiveURL, modSubdir: modSubdir }
}

const packages = [
  M("cloudlands", "https://github.com/Treer/cloudlands/archive/master.zip", "environment"),
  M("dfcaverns", "https://github.com/FaceDeer/dfcaverns/archive/master.zip", "environment"),
  M("ethereal", "https://notabug.org/TenPlus1/ethereal/archive/master.zip", "environment"),

  M("builtin_item", "https://notabug.org/TenPlus1/builtin_item/archive/master.zip", "misc"),
  M("craftguide", "https://github.com/minetest-mods/craftguide/archive/master.zip", "misc"),
  M("glider", "https://github.com/CBugDCoder/glider/archive/master.zip", "misc"),
  M("balloonblocks", "https://github.com/TBSHEB/minetest-balloon-blocks/archive/master.zip", "misc"),

  M("character_creator", "https://github.com/minetest-mods/character_creator/archive/master.zip", "player"),
  M("hbsprint", "https://github.com/minetest-mods/hbsprint/archive/master.zip", "player"),
  M("headanim", "https://github.com/LoneWolfHT/headanim/archive/master.zip", "player"),
  M("hunger_ng", "https://gitlab.com/4w/hunger_ng/-/archive/master/hunger_ng-master.zip", "player"),
  M("show_wielded_item", "https://content.minetest.net/packages/Wuzzy/show_wielded_item/download/", "player"),
  M("skinsdb", "https://github.com/minetest-mods/skinsdb/archive/master.zip", "player"),
  M("wielded_light", "https://github.com/minetest-mods/wielded_light/archive/master.zip", "player"),
  M("visible_sneak", "https://github.com/Niwla23/visible_sneak/archive/master.zip", "player"),
  M("controls", "https://github.com/mt-mods/controls/archive/master.zip", "player"),

  M("mesecons", "https://github.com/minetest-mods/mesecons/archive/master.zip"),
  M("meseportals", "https://notabug.org/Piezo_/minetest-meseportals/archive/master.zip"),
  M("minetest_game", "https://github.com/minetest/minetest_game/archive/master.zip") //// Doing the thing
]