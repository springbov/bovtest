df_underworld_items = {}

local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath.."/config.lua")
dofile(modpath.."/doc.lua")

dofile(modpath.."/crystals_amethyst.lua")
dofile(modpath.."/glow_stone.lua")
dofile(modpath.."/slade.lua")
dofile(modpath.."/glowing_pit_plasma.lua")

dofile(modpath.."/puzzle_seal.lua")